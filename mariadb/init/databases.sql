# Create initial databases for Pylon on container creation phase
CREATE DATABASE IF NOT EXISTS `pylon`;
CREATE DATABASE IF NOT EXISTS `pylon-testing`;
