Nginx-PHP7.2-FPM-MariaDB
=============

This repo contains a recipe for making a [Docker](http://docker.io) container for Pylon, using Linux, Nginx PHP7.2-FPM and MariaDB.
To build, make sure you have Docker [installed](http://www.docker.io/gettingstarted/).

## And run the container, connecting port 7090:
```
docker-compose up -d
```
That's it!
Visit http://localhost:7090 in your webrowser.

## More docker

```
# sudo docker ps
```

Start/Stop
```
docker stop $(docker ps -a -q)
sudo docker stop 9887DAS121
sudo docker start 9887DAS121
```

## Authors

Created and maintained by [Alexandre Dias][author] (<alex.jm.dias_at_gmail.com>)

## License
GPL v3

[author]:                 https://github.com/saidatom

![](https://images.microbadger.com/badges/image/saidatom/lep.svg)
